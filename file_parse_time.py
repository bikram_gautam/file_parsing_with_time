import string
import time     #importing time library
text=open("plrabn12.txt","r") #opening given file in read mode
data=text.read()   #reading characters
d=dict()   #creating empty dictionary
start=time.time()   #takes current time   
words=data.translate(data.maketrans("","",string.punctuation)).split() 
print("number of words",len(words)) #printing total count of words
print("number of characters",len(data)) #printing total count of characters
end=time.time() #takes end time
print("time:",(end-start)) #subtracting to get execution time
